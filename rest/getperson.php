<?php

$people = Array('Konrad' => [
        'name' => 'Konrad',
        'familyname' => 'P',
        'role' => 'God',
        'salary' => '999999999999999999999999999 $'
    ], 'Łukasz' => [
        'name' => 'Łukasz',
        'familyname' => 'S',
        'role' => 'Son Of God',
        'salary' => '999999999999999999999 $'
    ], 'Magdalena' => [
        'name' => 'Magdalena',
        'familyname' => 'L',
        'role' => 'Holy Spirit',
        'salary' => '99999 $'
    ], 'Hans' => [
        'name' => 'Hans',
        'familyname' => 'von Mahalitz',
        'role' => 'Master of Hell',
        'salary' => "so much souls you can't even imagine"
    ], 'Ula' => [
        'name' => 'Ula',
        'familyname' => 'S',
        'role' => 'archangel',
        'salary' => 'dwanaście groszy'
        ]);

header('Content-Type: application/json');
echo json_encode($people[$_GET['name']]);
