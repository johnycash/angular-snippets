servicesApp.factory('dataholder', function () {

  /*
   * Zmienne prywatne
   */

  var filters = {};
  var options = {
    'Gatunek muzyczny': ['Rock', 'Punk', 'Pop', 'Jazz', 'Blues', 'Classic'],
    'Lata': ['30', '60', '70', '80', '90', '00'],
    'Kontynent': ['Europa', 'Ameryka', 'Azja']
  }
  var bands = {
    "Glenn Miller": {'Gatunek muzyczny': 'Jazz', 'Lata': '30', 'Kontynent': 'Ameryka'},
    "Jethro Tull": {'Gatunek muzyczny': 'Rock', 'Lata': '60', 'Kontynent': 'Europa'},
    "King Crimson": {'Gatunek muzyczny': 'Rock', 'Lata': '60', 'Kontynent': 'Europa'},
    "Pink Floyd": {'Gatunek muzyczny': 'Rock', 'Lata': '60', 'Kontynent': 'Europa'},
    "The Beatles": {'Gatunek muzyczny': 'Pop', 'Lata': '60', 'Kontynent': 'Europa'},
    "The Doors": {'Gatunek muzyczny': 'Rock', 'Lata': '60', 'Kontynent': 'Ameryka'},
    "Sex Pistols": {'Gatunek muzyczny': 'Punk', 'Lata': '70', 'Kontynent': 'Europa'},
    "Proletaryat": {'Gatunek muzyczny': 'Punk', 'Lata': '80', 'Kontynent': 'Europa'},
    "Acidman": {'Gatunek muzyczny': 'Punk', 'Lata': '90', 'Kontynent': 'Azja'},
    "The Pillows": {'Gatunek muzyczny': 'Rock', 'Lata': '80', 'Kontynent': 'Azja'},
  };
  
  /*
   * Interfejs publiczny
   */

  var dataholderService = {
    setFilters: function (newfilters) {
      if (Object.keys(newfilters).length != 0) {
        filters = newfilters;
      }
      return filters;
    },
    getFilters: function () {
      return filters;
    },
    getOptions: function () {
      return options;
    },
    addBands: function (bandCollection) {
      for (i in bandCollection) {
        bands[i] = bandCollection[i]
      }
    },
    getDataCount: function () {
      return Object.keys(bands).length;
    },
    getData: function () {
      /*
       * Idiotyczne filtrowanie
       */
      var localBands = {};
      for (band in bands) {
        if (
                (bands[band]['Gatunek muzyczny'] == filters['Gatunek muzyczny'] || filters['Gatunek muzyczny'] == '')
                && (bands[band]['Lata'] == filters['Lata'] || filters['Lata'] == '')
                && (bands[band]['Kontynent'] == filters['Kontynent'] || filters['Kontynent'] == '')
                ) {
          localBands[band] = bands[band];
        }
      }

      return localBands;
    }
  };
  return dataholderService;
});