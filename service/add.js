servicesApp.directive('add', ['dataholder', function (dataholder) {
    return {
      templateUrl: 'service/add.html',
      restrict: 'AEC',
      scope: {},
      controller: function ($scope) {
        $scope.bandData = {};
        $scope.bandName = '';

        $scope.getOptions = function () { //potrzebne w szablonie
          return dataholder.getOptions();
        };

        $scope.addBand = function () {
          /*
           * Dataholder akceptuje dane w formie kolekcji obiektów. Zbudujmy ją i wprowadźmy do dataholdera!
           */
          var bandCollection = {};
          bandCollection[$scope.bandName] = $scope.bandData;
          dataholder.addBands(bandCollection);
          $scope.bandName = '';
          $scope.bandData = {};
        };
      }


    };
  }]);