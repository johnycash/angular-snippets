servicesApp.directive('viewer', ['dataholder', function (dataholder) {
    return {
      templateUrl: 'service/viewer.html',
      restrict: 'AEC',
      scope: {},
      controller: function ($scope) {
        $scope.bands = [];

        /*
         * DING DONG! Zapoznajemy się z $watchCollection! Bada on składniki obiektu, w odróżnieniu od $watch,
         *  który sprawdza tylko, czy obiekt nadal jest sobą, czy może w jego miejsce wprowadzono inny.
         */
        
        $scope.$watchCollection(function () { //badamy czy zmieniła się wartość zwracana przez tę funkcję
          var watchedObject = dataholder.getFilters();
          watchedObject.dataCount = dataholder.getDataCount();
          return watchedObject;
        }, function (nval, oval) {
          $scope.bands = dataholder.getData();
        });
        
      }


    };
  }]);

