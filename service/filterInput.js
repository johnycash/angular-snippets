servicesApp.directive('filterInput', ['dataholder', function (dataholder) {
    return {
      templateUrl: 'service/filterInput.html',
      restrict: 'AEC',
      scope: {},
      controller: function ($scope) {
        $scope.getOptions = function () {
          return dataholder.getOptions();
        };
        $scope.filters = {};
        for (i in $scope.getOptions()) {
          $scope.filters[i] = '';
        }

        /*
         * DING DONG! Zapoznajemy się z $watchCollection! Bada on składniki obiektu, w odróżnieniu od $watch,
         *  który sprawdza tylko, czy obiekt nadal jest sobą, czy może w jego miejsce wprowadzono inny.
         */
        $scope.$watchCollection('filters', function (nval, oval) {
          dataholder.setFilters(nval);
        });
      }
    };
  }]);