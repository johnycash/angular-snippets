resourceApp.directive('stopwatchList', ['stopwatch', '$interval', function (stopwatch, $interval) {
    return {
      templateUrl: 'resource/stopwatchList/stopwatchList.html',
      restrict: 'AEC',
      scope: {
        watches: '='
      },
      controller: function ($scope) {
        
        $scope.updateList = function () {
          stopwatch.query({}, function (data) {
            for (i = 0; i < data.length; i++) {
              //dodajemy tylko nowe elementy, stare same o siebie zadbają.
              if (typeof ($scope.watches[i]) == 'undefined') {
                $scope.watches[i] = data[i];
              }
            }
          });
        }

        $scope.updateList();

        $interval($scope.updateList, 10000);
      }
    };
  }]);