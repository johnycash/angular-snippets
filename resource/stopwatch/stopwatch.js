resourceApp.directive('stopwatch', ['stopwatch', '$timeout', '$interval', function (stopwatch, $timeout, $interval) {
    return {
      templateUrl: 'resource/stopwatch/stopwatch.html',
      restrict: 'AEC',
      scope: {
        ngWatch: '='
      },
      controller: function ($scope) {
        $scope.timeout = {};

        $scope.isOn = false;

        $scope.tick = function () {
          /* 
           * po usunięciu komentarza ekran zacznie skakać - po zrealizowaniu obietnicy wszystko będzie OK, 
           * ale zaraz po wywołaniu funkcji get do zmiennej zostanie przypisana obietnica
           * i angular będzie próbował ją wyświetlić, co spowoduje miganie ekranu.
           */
          
          //$scope.ngWatch = 
          stopwatch.get({name: $scope.ngWatch.name}, function (stopwatch) {
            $scope.ngWatch = stopwatch;
            $scope.ngWatch.seconds++;
            $scope.ngWatch.$update();
          });

          if ($scope.isOn) {
            $scope.timeout = $timeout($scope.tick, 1000);
          }
        }

        $scope.startTicking = function () {
          $scope.timeout = $timeout($scope.tick, 1000);
          $scope.isOn = true;
        }

        $scope.stopTicking = function () {
          $scope.isOn = false;
        }

        /*
         * działa, ponieważ funkcja get zwraca obietnicę, która potem zwraca odpowiedni obiekt
         */
        $scope.ngWatch = stopwatch.get({name: $scope.ngWatch.name});
        
        $scope.refreshData = $interval(function(){
          stopwatch.get({name: $scope.ngWatch.name}, function (stopwatch) {
            $scope.ngWatch = stopwatch;
          })
        }, 10000);

        $scope.$on('$destroy', function () {
          $scope.isOn = false;
        });
      }
    };
  }]);