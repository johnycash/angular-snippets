resourceApp.directive('stopwatchCreator', ['stopwatch', function (stopwatch) {
    return {
      templateUrl: 'resource/stopwatchCreator/stopwatchCreator.html',
      restrict: 'AEC',
      scope: {
        watches: '='
      },
      controller: function ($scope) {
        
        $scope.name = '';
        
        $scope.create = function(){
          newStopwatch = new stopwatch(); //tworzymy obiekt
          console.log(newStopwatch);

          newStopwatch.name = $scope.name; //wypełniamy go danymi
          newStopwatch.seconds = 0;
          
          newStopwatch.$save(function(){
            $scope.name = ''; //czyścimy scope dla następnego obiektu
            $scope.watches = stopwatch.query(); //aktualizujemy listę obiektów w nadrzędnym scopie, czyli również w liście
          });
        }
      }
    };
  }]);