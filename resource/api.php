<?php //NIE DOTYKAĆ, NIE RUSZAĆ... JEDNYM SŁOWEM SPIERDALAĆ!!

header('Content-type: application/json');

$getParams = explode('/', $_SERVER['PATH_INFO']);

//obiekty danego typu są trzymane w jsonie w odpowiednim pliku,
//pod [2] masz typ obiektu, wczytaj wszystkie tego typu do tablicy
if (file_exists('data/' . $getParams[1] . '.json')) {
  $jsonString = file_get_contents('./data/' . $getParams[1] . '.json');
  $data = json_decode($jsonString);
  if ($data == null)
    $data = [];
} else {
  die;
}

if ($_SERVER['REQUEST_METHOD'] == 'GET') {

  if (count($getParams) == 3) {
    //pobierz jeden obiekt, pod [2] masz jego nazwę
    foreach ($data as $item) {
      if ($item->name == $getParams[2]) {
        echo json_encode($item, JSON_FORCE_OBJECT);
      }
    }
  } else {
    echo $jsonString;
  }
} else if ($_SERVER['REQUEST_METHOD'] == 'PUT') {
  //updatuje obiekt
  $newObject = json_decode(file_get_contents("php://input"));

  if ($newObject->seconds == NULL || $newObject->name == null) {
    header("HTTP/1.1 500 Internal Server Error");
    die;
  }

  foreach ($data as &$item) {
    if ($item->name == $newObject->name) {
      $item = $newObject;
    }
  }

  file_put_contents('./data/' . $getParams[1] . '.json', json_encode($data));

  echo json_encode($newObject, JSON_FORCE_OBJECT);
} else if ($_SERVER['REQUEST_METHOD'] == 'POST') {
  //tworzy nowy obiekt
  $newObject = json_decode(file_get_contents("php://input"));

  foreach ($data as $item) {
    if ($item->name == $newObject->name) {
      header("HTTP/1.1 500 Internal Server Error");
      die;
    }
  }

  $data[] = $newObject;
  file_put_contents('./data/' . $getParams[1] . '.json', json_encode($data));

  echo json_encode($newObject, JSON_FORCE_OBJECT);
} else if ($_SERVER['REQUEST_METHOD'] == 'DELETE') {
  //kasowanie w toku, nie testowane
  $newObject = json_decode(file_get_contents("php://input"));

  if ($newObject->seconds == NULL || $newObject->name == null) {
    header("HTTP/1.1 500 Internal Server Error");
    die;
  }

  foreach ($data as &$item) {
    if ($item->name == $newObject->name) {
      unset($item);
    }
  }

  file_put_contents('./data/' . $getParams[1] . '.json', json_encode($data));
  echo json_encode($newObject, JSON_FORCE_OBJECT);
}